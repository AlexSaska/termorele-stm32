/**
  ******************************************************************************
  * @file    main.c
  * @author  Ac6
  * @version V1.0
  * @date    01-December-2013
  * @brief   Default main function.
  ******************************************************************************
*/


#include "stm32f0xx.h"
//#include "ds18b20.h"
//#include "eeprom.h"
#include <stdio.h>
#include <stdlib.h>

#define SPI1_DR_8b          (*(__IO uint8_t *)((uint32_t)SPI1 + 0x0C))

void delay_ms(uint32_t time);
void RCC_Init(void);
void Timer16_Init(void);
void USART1_Init(void);
void USART1_send_char(char chr);
void USART1_send_string(char* str);
void SPI1_Init(void);
void SPI_send_byte(uint8_t data);
void menu(void);
void button(void);

const uint8_t num[12] =
{
		0xc0, //0
		0xf9, //1
		0xa4, //2
		0xb0, //3
		0x99, //4
		0x92, //5
		0x82, //6
		0xf8, //7
		0x80, //8
		0x90, //9
		0xbf, //-
		0x8c, //P
};

const uint8_t digit[4] =  // 1-2-3-4
{
		0x08, //1
		0x04, //2
		0x02, //3
		0x01, //4
};

volatile uint8_t buff[4] = {0x8c, 0xbf, 0xc0, 0x80};
volatile uint8_t index = 0;

void Timer16_Init(void)
{
	RCC->APB2ENR |= RCC_APB2ENR_TIM16EN;
	TIM16->PSC = 16000-1;
	TIM16->ARR = 2;
	TIM16->DIER |= TIM_DIER_UIE;
	TIM16->CR1 |= TIM_CR1_CEN;
	NVIC_EnableIRQ(TIM16_IRQn);
	NVIC_SetPriority(TIM16_IRQn,0);
}

void TIM16_IRQHandler(void)
{
	TIM16->SR &= ~TIM_SR_UIF;
	GPIOA->BSRR |= GPIO_BSRR_BR_4;
	SPI_send_byte(digit[index]);
	SPI_send_byte(buff[index]);
	GPIOA->BSRR |= GPIO_BSRR_BS_4;
	index++;
	if(index >= 4)
	{
		index = 0;
	}
}

void delay_ms(uint32_t time)
{
	while(time--)
	{
		;
	}
}

void RCC_Init(void)
{
	RCC->CR |= RCC_CR_HSEON; 
	while (!(RCC->CR & RCC_CR_HSERDY)) {}   
	FLASH->ACR = FLASH_ACR_PRFTBE | FLASH_ACR_LATENCY; 
	RCC->CFGR |= RCC_CFGR_HPRE_DIV1; 
	RCC->CFGR |= RCC_CFGR_PPRE_DIV1; 
	RCC->CFGR &= ~RCC_CFGR_PLLMULL; 
	RCC->CFGR &= ~RCC_CFGR_PLLSRC; 
	RCC->CFGR &= ~RCC_CFGR_PLLXTPRE; 
	RCC->CFGR |= RCC_CFGR_PLLSRC_HSE_PREDIV; 
	RCC->CFGR |= RCC_CFGR_PLLXTPRE_PREDIV1_Div2; 
	RCC->CFGR |= RCC_CFGR_PLLMULL4; //24MHz
	RCC->CR |= RCC_CR_PLLON; 
	while((RCC->CR & RCC_CR_PLLRDY) == 0) {} 
	RCC->CFGR &= ~RCC_CFGR_SW; 
	RCC->CFGR |= RCC_CFGR_SW_HSE; 
	while((RCC->CFGR & RCC_CFGR_SWS) != RCC_CFGR_SWS_HSE) {} 

	RCC->APB2ENR = RCC_APB2ENR_SYSCFGEN;
	RCC->CFGR3 |= RCC_CFGR3_I2C1SW; //sysclk 16mhz
	RCC->APB1ENR |= RCC_APB1ENR_I2C1EN;
	RCC->APB2ENR |= RCC_APB2ENR_USART1EN | RCC_APB2ENR_SPI1EN;// |RCC_APB1ENR_I2C1EN ;
	RCC->AHBENR |= RCC_AHBENR_GPIOAEN | RCC_AHBENR_GPIOBEN;
}

void USART1_Init(void)
{
	//TX - PA2, RX - PA3 AFIO1
	GPIOA->MODER &= ~GPIO_MODER_MODER2;
	GPIOA->MODER |= GPIO_MODER_MODER2_1;
	GPIOA->AFR[0] |=  0x100;

	USART1->CR1 &= ~USART_CR1_OVER8;
	USART1->BRR |= 0x341; //16MHz  19200
	USART1->CR1 |= USART_CR1_TE;
	USART1->CR1 |= USART_CR1_UE;
}

void USART1_send_char(char chr)
{
	while (!(USART1->ISR & USART_ISR_TC));
	USART1->TDR = chr;
}

void USART1_send_string(char* str)
{
	uint8_t i = 0;
	while(str[i])
	{
		USART1_send_char(str[i++]);
	}
}

void SPI1_Init(void)
{
	//cs  pa4
	GPIOA->MODER |= GPIO_MODER_MODER4_0;
	GPIOA->OTYPER &= ~GPIO_OTYPER_OT_4;
	GPIOA->OSPEEDR |= GPIO_OSPEEDR_OSPEEDR4;
	GPIOA->PUPDR |= GPIO_PUPDR_PUPDR4_0;
	GPIOA->BSRR |= GPIO_BSRR_BS_4;
	//sck pa5
	GPIOA->MODER &= ~GPIO_MODER_MODER5;
	GPIOA->MODER |= GPIO_MODER_MODER5_1;
	GPIOA->OSPEEDR |= GPIO_OSPEEDR_OSPEEDR5;
	//mosi pa7
	GPIOA->MODER &= ~GPIO_MODER_MODER7;
	GPIOA->MODER |= GPIO_MODER_MODER7_1;
	GPIOA->OSPEEDR |= GPIO_OSPEEDR_OSPEEDR7;

	SPI1->CR1 |= SPI_CR1_BR_0;         // 4MHz
	SPI1->CR1 |= SPI_CR1_CPOL | SPI_CR1_CPHA;
	SPI1->CR1 |= SPI_CR1_BIDIMODE;
	SPI1->CR1 |=  SPI_CR1_BIDIOE;
	SPI1->CR1 &= ~SPI_CR1_LSBFIRST;
	SPI1->CR1 |= SPI_CR1_SSM;
	SPI1->CR1 |= SPI_CR1_MSTR;
	SPI1->CR2 &= ~SPI_CR2_DS;
	//SPI1->CR2 |= SPI_CR2_DS_0 | SPI_CR2_DS_1 | SPI_CR2_DS_2;
	SPI1->CR2 |= SPI_CR2_SSOE;
	SPI1->CR1 |= SPI_CR1_SPE;
}

void SPI_send_byte(uint8_t data)
{
	while (!(SPI1->SR & SPI_SR_TXE)){};    
	SPI1_DR_8b = data;                               
	while (SPI1->SR & SPI_SR_BSY){};           
}

void  menu(void)
{

}

void button(void)
{
	if((GPIOA->IDR & GPIO_IDR_14) != 0)
	{
		USART1_send_string("btn1 press");
		delay_ms(1000000);
	}
	if((GPIOA->IDR & GPIO_IDR_13) != 0)
	{
		USART1_send_string("btn2 press");
		delay_ms(1000000);
	}
	if((GPIOB->IDR & GPIO_IDR_1) != 0)
	{
		USART1_send_string("btn3 press\r\n");
		delay_ms(1000000);
	}
}


volatile uint8_t i = 0;
uint8_t t;
char p[34];

int main(void)
{
	__disable_irq();
	RCC_Init();
	USART1_Init();
	SPI1_Init();
	Timer16_Init();
	//EEPROM_Init();
	//pa1 - button
	GPIOA->MODER &= ~(GPIO_MODER_MODER14 | GPIO_MODER_MODER13);
	GPIOA->PUPDR &= ~(GPIO_PUPDR_PUPDR14 | GPIO_PUPDR_PUPDR13);
	GPIOA->PUPDR |= GPIO_PUPDR_PUPDR14_1 | GPIO_PUPDR_PUPDR13_1;
	GPIOA->OSPEEDR |= (GPIO_OSPEEDR_OSPEEDR14 | GPIO_OSPEEDR_OSPEEDR13);

	GPIOB->MODER &= ~GPIO_MODER_MODER1;
	GPIOB->PUPDR &= ~GPIO_PUPDR_PUPDR1;
	GPIOB->PUPDR |= GPIO_PUPDR_PUPDR1_1;
	GPIOB->OSPEEDR |= GPIO_OSPEEDR_OSPEEDR1;

	//DS18B20_init(ds18b20_res_9 );
	USART1_send_string("mcu init OK\r\n");
	__enable_irq();

	while(1)
	{
		//USART1_send_string("mcu init OK\r\n");
		/*if((GPIOA->IDR & GPIO_IDR_1) == 0)
		{
			i = 1;
			delay_ms(1000000);
		}

		if(i == 1)
		{
			//t = DS18B20_getTemp();
			//itoa(t,p,10);
			//USART1_send_string(p);
			//uint8_t data = EEPROM_read_byte(0xae, 0x21);

			buff[0] = num[1];
			buff[1] = num[3];
			buff[2] = num[4];
			buff[3] = num[7];
			delay_ms(1000);
			i = 0;
		}*/
		button();
	}
}
